module HYSRF05(){
    import("C:/Users/mobil/OneDrive/Plocha/HY-SRF05_v3.stl");
}
module TwoSlopes(depth, diameter, distance)
{
    translate([0,distance/2,0])cylinder(depth,d=diameter, center= true);
    translate([0,-distance/2,0])cylinder(depth,d=diameter, center= true);
}

module screwHoles()
{
    TwoSlopes(20,3,60);
    translate([0,0,2])TwoSlopes(2,6,60);
}

module HYSRF05Holes()
{
    TwoSlopes(20,17,25);
}
$fn=150;

module HYSRF05Front()
{
    difference()
    {
        union(){
            hull()
            {
                translate([-33,0,0])cylinder(30,d=3, center= true);
                translate([33,0,0])cylinder(30,d=3, center= true);
            }
            hull()
            {
                translate([-33,0,0])cylinder(30,d=3, center= true);
                translate([-78,-25,0])cylinder(30,d=3, center= true);
            }
            hull()
            {
                translate([33,0,0])cylinder(30,d=3, center= true);
                translate([78,-25,0])cylinder(30,d=3, center= true);
            }
        }
        

            rotate([0,90,90])HYSRF05Holes();
        translate([0,0,10]){
            rotate([0,90,90])screwHoles();
        }
        translate([-53, -20, 0])rotate([0,0,210])rotate([0,90,90])HYSRF05Holes();
        translate([53, -20, 0])rotate([0,180,150])rotate([0,90,90])HYSRF05Holes();
    }
}
translate([0,0,-15])hull(){

    translate([-33,0,0])cylinder(2,d=3, center= true);
    translate([33,0,0])cylinder(2,d=3, center= true);


    translate([-78,-25,0])cylinder(2,d=3, center= true);

    translate([78,-25,0])cylinder(2,d=3, center= true);
           
}

HYSRF05Front();

