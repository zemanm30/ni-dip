
module rectStand(r, height, width, lenght)
{
    translate([width/2, lenght/2, 0])cylinder (height, r, r, center = true);
    translate([-width/2, lenght/2, 0])cylinder (height, r, r, center = true);
    translate([width/2, -lenght/2, 0])cylinder (height, r, r, center = true);
    translate([-width/2, -lenght/2, 0])cylinder (height, r, r, center = true);
}

module ArduinoStand(type="UNO", r, h){
    translate([0, 0, 0])cylinder (h, r, r, center = true);
    translate([1.3, 47.5, 0])cylinder (h, r, r, center = true);
    translate([-50.8, 15.2, 0])cylinder (h, r, r, center = true);
    translate([-50.8, 43.1, 0])cylinder (h, r, r, center = true);
    if(type=="MEGA")
    {
       translate([-74.9, 0, 0])cylinder (h, r, r, center = true);
       translate([-82.5, 47.5, 0])cylinder (h, r, r, center = true);
    }
}
