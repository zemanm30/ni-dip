$fn=50;
difference(){
    union(){
        cube ([44,44,15],center=true);
        translate([1,0,0]){
            translate([18,18,2]){
                cylinder(h=15,r1=3,r2=3,center=true);
            };
            translate([-19,18,2]){
                cylinder(h=15,r1=3,r2=3,center=true);
            };
            translate([18,-19,2]){
                cylinder(h=15,r1=3,r2=3,center=true);
            };
            translate([-19,-19,2]){
                cylinder(h=15,r1=3,r2=3,center=true);
            };
        }
        translate([-21,-25,0]){
            translate([1.5,0,6.5]){
                cube([5,22,2], center=true);
            }
            translate([-4,-8,0]){
            difference(){
                union(){
                    translate([-3,-3,5.5]){cube([6,6,2],centre=true);}
                    translate([0,0,1]){cylinder(h=16.57,r1=3,r2=3,center=true);}
                }
                cylinder(h=25,r1=2,r2=2,center=true);
                }
            }
        }
        
    }
    translate([1,0,0]){
        translate([18,18,2]){
            cylinder(h=25,r1=2,r2=2,center=true);
        };
        translate([-19,18,2]){
            cylinder(h=25,r1=2,r2=2,center=true);
        };
        translate([18,-19,2]){
            cylinder(h=25,r1=2,r2=2,center=true);
        };
        translate([-19,-19,2]){
            cylinder(h=25,r1=2,r2=2,center=true);
        };
        
        translate([-10,18,2]){
            cylinder(h=25,r1=2,r2=2,center=true);
        };
        
        translate([18,18,-2]){
            cylinder(h=15,r1=3,r2=3,center=true);
        };
        translate([-19,18,-2]){
            cylinder(h=15,r1=3,r2=3,center=true);
        };
        translate([18,-19,-2]){
            cylinder(h=15,r1=3,r2=3,center=true);
        };
        translate([-19,-19,-2]){
            cylinder(h=15,r1=3,r2=3,center=true);
        };
}
    translate([0,0,-2]){
        cube ([40,44,15],center=true);
    };
    translate([0,-2,0]){
        cube ([30,30,16],center=true);
    };
    //translate([0,0,0]){
    //    cube ([60,30,10],center=true);
    //};
    translate([0,40,-2]){
        cube ([45,44,15],center=true);
    };
    
    translate([5,-40,-2]){
        cube ([45,44,15],center=true);
    };
}
