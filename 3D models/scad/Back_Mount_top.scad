include <common.scad>
$fn = 50;

//battery stand
module batteryStand(){
    difference()
    {
        translate([-75,0,5])
        {
            hull()
            {
            rectStand(3, 10, 30, 150);
            }
        }
        #translate([-95,0,49])#cube([65, 150, 95], center = true);
    }
}

module plane()
{
    //plane
    hull()
    {
        rectStand(3, 3, 150, 150);
        translate([77,0,0])rectStand(3, 3, 50, 50);
        translate([-15,0,0])rectStand(3, 3, 150, 150);
    }
}

difference(){
    
    union()
    {
        difference()
        {
            union()
            {
                    plane();
                    batteryStand();
            }
    
            //screw holes
            #rectStand(1.5, 5, 150, 100);
            #translate([0,0,2])rectStand(3, 3, 150, 100);
        }
        translate([40, 20, 1.5])ArduinoStand("MEGA",3 , 4.5);
        translate([40, -70, 1.5])ArduinoStand("MEGA",3 , 4.5);
        
    }
    
    #translate([0,0,0]){
        #cube([120, 20, 95], center = true);
    }
    #translate([90,0,0]){
        #cube([20, 40, 95], center = true);
    }
    
    
    //power /can
    #translate([-50,-45,0]){
        #cube([18, 30, 95], center = true);
    }
    #translate([-50,45,0]){
        #cube([18, 30, 95], center = true);
    }
    
    #translate([60,45,0]){
        #cube([18, 30, 95], center = true);
    }
    
    #translate([60,-45,0]){
        #cube([18, 30, 95], center = true);
    }
    #translate([40, 20, 0])#ArduinoStand("MEGA",1.7 , 10);
    #translate([40, -70, 0])#ArduinoStand("MEGA",1.7 , 10);
}