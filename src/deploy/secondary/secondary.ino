#include <util/atomic.h>
#include <SPI.h>
#include "mcp2515_can.h"

#include "motor_cfg.h"
#include "CAN_cfg.h"


volatile int posa = 0, posb = 0;
volatile int speeda = 0, speedb = 0;
//pwm of each side
volatile uint16_t pwma = 0;
volatile uint16_t pwmb = 0;

//target speed of each side
volatile int16_t targetspeeda = 0;
volatile int16_t targetspeedb = 0;

volatile int16_t newspeeda = 0;
volatile int16_t newspeedb = 0;

//mask for the slave
const uint16_t slave_mask = CAN_DIR_BIT | CAN_ADDR_BITS;

//slave address, can't be 0 -> would be master
const uint16_t addr = 1;

mcp2515_can CAN(9);  // Set CS pin

//CAN msg recieve buffer
unsigned long ID = 0;
unsigned char len = 0;
unsigned char buf[4];
unsigned char sbuf[8];

//CAN message timeout in 10micro sec
constexpr uint64_t TIMEOUT_PERIOD = 2;

volatile uint64_t timeoutcount = 0;

int error = 0;

void setup() {


  Serial.begin(9600);

  //initialize inputs
  pinMode(ENCLA, INPUT);
  pinMode(ENCLB, INPUT);
  pinMode(ENCRA, INPUT);
  pinMode(ENCRB, INPUT);

  pinMode(PWMA, OUTPUT);
  pinMode(DIRAF, OUTPUT);
  pinMode(DIRAB, OUTPUT);

  pinMode(PWMB, OUTPUT);
  pinMode(DIRBF, OUTPUT);
  pinMode(DIRBB, OUTPUT);

  digitalWrite(DIRAF, 0);
  digitalWrite(DIRAB, 0);
  analogWrite(PWMA, 0);

  digitalWrite(DIRBF, 0);
  digitalWrite(DIRBB, 0);
  analogWrite(PWMB, 0);

  //attach encoder interrupts
  attachInterrupt(digitalPinToInterrupt(ENCLA), readLEncoder, RISING);
  attachInterrupt(digitalPinToInterrupt(ENCRA), readREncoder, RISING);

  attachInterrupt(digitalPinToInterrupt(CAN_INT_PIN), MCP2515_ISR, FALLING);
  while (CAN_OK != CAN.begin(CAN_500KBPS)) {
    Serial.println("init");
    delay(100);
  }

  cli();  //stop interrupts
  //set timer1 interrupt at 10Hz
  TCCR1A = 0;  // set entire TCCR1A register to 0
  TCCR1B = 0;  // same for TCCR1B
  TCNT1 = 0;   //initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 6245;  // = (16*10^6) / (10*256) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 bits for 256 prescaler
  TCCR1B |= (1 << CS12);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();  //allow interrupts

  CAN.init_Mask(0, 0, slave_mask);
  CAN.init_Filt(0, 0, CAN_DIR_BIT);
  CAN.init_Filt(1, 0, (CAN_DIR_BIT | (CAN_ADDR_BITS & addr)));

  error = 0;
}

void loop() {
  //check DIAG pins
  int vall = analogRead(DIAGL);
  int valr = analogRead(DIAGR);

  if (valr < 700 || vall < 700) {
    error = 1;
  }
  delay(500);
}

void MCP2515_ISR() {
  while (CAN_MSGAVAIL == CAN.checkReceive()) {

    //move data to buffer
    CAN.readMsgBufID(&ID, &len, buf);

    if ((ID & 0xF) == 0 || (ID & 0xF) == addr) {
      //process msg
      switch ((ID) >> 5) {
        case CAN_MSG_SSPEED:
          if (len == 4) {
            newspeeda = (buf[0]) | (buf[1] << 8);
            newspeedb = (buf[2]) | (buf[3] << 8);

            targetspeeda = newspeeda;
            targetspeedb = newspeedb;
          }
          if (error != 0) {
            //error
            sbuf[0] = error;
            CAN.sendMsgBuf(CAN_MSG_ERR << 5 | (addr & 0xF), 0, 1, sbuf);
          }
          break;
        case CAN_MSG_ERR:
          //set to error
          error = 2;  //external error
          if (len == 1) {
            //reset error
            error = buf[0];
          }
          break;
        case CAN_MSG_STOP:
          targetspeeda = 0;
          targetspeedb = 0;
          break;
      };

      //send back response

      timeoutcount = 0;
    }
  }
}

ISR(TIMER1_COMPA_vect) {
  speeda = posa * 10;
  speedb = posb * 10;

  posa = 0;
  posb = 0;

  //stop when the CAN timeouts
  if (timeoutcount >= TIMEOUT_PERIOD || error != 0) {
    targetspeeda = 0;
    targetspeedb = 0;
  } else {
    timeoutcount++;
  }

  //calculate pwma
  if (abs(speeda) < abs(targetspeeda)) {
    if (abs(abs(speeda) - abs(targetspeeda)) > 200) {
      pwma += 10;
    }
    if (abs(abs(speeda) - abs(targetspeeda)) > 50) {
      pwma++;
    }
    if (pwma > 255) {
      pwma = 255;
    }
  } else if (abs(speeda) > abs(targetspeeda)) {
    if (abs(abs(speeda) - abs(targetspeeda)) > 200) {
      pwma -= 10;
    } else if (abs(abs(speeda) - abs(targetspeeda)) > 50) {
      pwma--;
    }
    if (pwma > 255) {
      pwma = 0;
    }
  }

  if (abs(speedb) < abs(targetspeedb)) {
    if (abs(abs(speedb) - abs(targetspeedb)) > 200) {
      pwmb += 10;
    } else if (abs(abs(speedb) - abs(targetspeedb)) > 50) {
      pwmb++;
    }
    if (pwmb > 255) {
      pwmb = 255;
    }
  } else if (abs(speedb) > abs(targetspeedb)) {
    if (abs(abs(speedb) - abs(targetspeedb)) > 200) {
      pwmb -= 10;
    } else if (abs(abs(speedb) - abs(targetspeedb)) > 50) {
      pwmb--;
    }
    if (pwmb > 255) {
      pwmb = 0;
    }
  }

  //set dira
  if (targetspeeda > 0) {
    digitalWrite(DIRAF, 0);
    digitalWrite(DIRAB, 1);
  } else if (targetspeeda < 0) {
    digitalWrite(DIRAF, 1);
    digitalWrite(DIRAB, 0);
  } else {
    digitalWrite(DIRAF, 0);
    digitalWrite(DIRAB, 0);
    pwma = 0;
  }

  //set dirb
  if (targetspeedb > 0) {
    digitalWrite(DIRBF, 0);
    digitalWrite(DIRBB, 1);
  } else if (targetspeedb < 0) {
    digitalWrite(DIRBF, 1);
    digitalWrite(DIRBB, 0);
  } else {
    digitalWrite(DIRBF, 0);
    digitalWrite(DIRBB, 0);
    pwmb = 0;
  }
  analogWrite(PWMB, pwmb);
  analogWrite(PWMA, pwma);
  

  if (error == 0) {
    //CAN.sendMsgBuf();
    sbuf[0] = (targetspeeda & 0xFF);
    sbuf[1] = ((targetspeeda >> 8) & 0xFF);
    sbuf[2] = (targetspeedb & 0xFF);
    sbuf[3] = ((targetspeedb >> 8) & 0xFF);
    CAN.sendMsgBuf(CAN_MSG_SSPEED << 5 | (addr), 0, 4, sbuf);
  } else {
    //error
    sbuf[0] = error;
    CAN.sendMsgBuf(CAN_MSG_ERR << 5 | (addr), 0, 1, sbuf);
  }
}

void readLEncoder() {
  int b = digitalRead(ENCLB);
  if (b > 0) {
    posa++;
  } else {
    posa--;
  }
}

void readREncoder() {
  int b = digitalRead(ENCRB);
  if (b > 0) {
    posb++;
  } else {
    posb--;
  }
}