

#ifndef __HY_SRF05_H__
#define __HY_SRF05_H__

#define TRIGG_FRONT_1 14
#define ECHO_FRONT_1 15

#define TRIGG_FRONT_2 48
#define ECHO_FRONT_2 49

#define TRIGG_FRONT_3 46
#define ECHO_FRONT_3 47

#define TRIGG_BACK_1 44
#define ECHO_BACK_1 45

#define TRIGG_BACK_2 42
#define ECHO_BACK_2 43

#define TRIGG_BACK_3 40
#define ECHO_BACK_3 41

#endif