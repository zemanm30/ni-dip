
#ifndef __RECIEVER_C__
#define __RECIEVER_C__

#include "Arduino.h"

/**
 * Measures the wave on specified pin and returns number between -20 and 20,
 * which corresponds to the position of the peg on transciever.
 * Returns -32 in case of failure  
 */
int RECReadRecieverPin(int recieverPin) {
  int in = pulseIn(recieverPin, HIGH, 50000);

  if (in == 0) {
    return -32;
  } else {
    return ( (in / 10) - 145) / 2;
  }
}

/**
 * Calculate value of left part of vehicle from two coordinance
 */
int RECCalculateLeft(int x, int y) {
  if (x == -32 || y == -32)
    return 0;

  int dist = min(sqrt(x * x + y * y), 20);

  if (x >= 0 && y >= 0) {
    return (y - x);
  }
  else if (x < 0 && y >= 0) {
    return (dist);
  }
  else if (x >= 0 && y < 0) {
    return (y + x);
  } else {
    return (-dist);    
  }
  return 0;
}

/**
 * Calculate value of right part of vehicle from two coordinance
 */
int RECCalculateRight(int x, int y) {
  if (x == -32 || y == -32)
    return 0;
  
  int dist = min(sqrt(x * x + y * y), 20);

  if (x >= 0 && y >= 0) {
    return (dist);
  }
  else if (x < 0 && y >= 0) {    
    return (y + x);
  }
  else if (x >= 0 && y < 0) {
    return (-dist);
  } else {
    return (y - x);
  }
  return 0;
}

#endif