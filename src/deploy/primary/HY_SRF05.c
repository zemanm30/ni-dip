#include "Arduino.h"

#ifndef __HY_SRF05_C__
#define __HY_SRF05_C__

/**
 * initialize pins, where HY-SRF05 is connected
 */
void initSensor(int trigg, int echo)
{
  pinMode(trigg, OUTPUT);
  pinMode(echo, INPUT);  
}

/**
 * triggers measuring sequence on HY-SRF05 and returns distance in cm
 */
int getDistance(int trigg, int echo)
{
  //start sequence
  digitalWrite(trigg, LOW);
  delayMicroseconds(2);
  digitalWrite(trigg, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigg, LOW);  

  unsigned long dur = pulseIn(echo, HIGH, 100000);

  return dur / 29 / 2;  
}

/**
  * from the mesuring of the sonsors in correct direction recount new speed
  */
int recountSpeed (int speed, int adjecent, int forward, int oposite)
{
  int fspeed = speed, opspeed = speed, adjspeed = speed;
  
  if (forward != 0 && forward <= 400)
  {  
    fspeed = (int) (((float) (speed)) * (((float) (forward - 25)) / 600.0));

    if (forward < 25)
    {
      fspeed = 0;      
    }    
  }
  if (adjecent != 0 && adjecent < 300)
  {
    adjspeed = (int) (((float) (speed)) * (((float) (adjecent)) / 300.0));
    if (adjecent < 20)
    {
      adjspeed = 0;      
    }
  }

  if (oposite != 0 && oposite < 300)
  {
    opspeed = (int) (((float) (speed)) * (((float) (oposite)) / 300));
    if (oposite < 20)
    {
      opspeed = 0;      
    }    
  }
  if (speed >= 0){
    return min(speed, min(fspeed, min(opspeed, adjspeed)));
  } 
  else
  {
    return max(speed, max(fspeed, max(opspeed, adjspeed)));
  }
}
#endif