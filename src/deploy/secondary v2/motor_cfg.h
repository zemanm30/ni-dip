
//input encoder pins
#define ENCLA 18
#define ENCLB 19
#define ENCRA 20
#define ENCRB 21

//motora pins
#define PWMAF 4
#define DIRAF 46

#define PWMAB 5
#define DIRAB 48

#define PWMBF 3
#define DIRBF 44

#define PWMBB 6
#define DIRBB 50

#define DIAGL A2
#define DIAGR A3