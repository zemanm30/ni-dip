#include <util/atomic.h> // For the ATOMIC_BLOCK macro

#define PWMA  5
#define DIRAF 7
#define DIRAB 8

#define PWMB  6
#define DIRBF 4
#define DIRBB 10

#define ENCA 18 // YELLOW
#define ENCB 19 // YELLOW

#define ENA A0


#define ERROR 10

volatile unsigned int distanceia = 0;
volatile unsigned int distanceib = 0;
unsigned int target = 1500;
uint8_t pwm = 128;

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600);
  
  pinMode (PWMA, OUTPUT);
  pinMode (DIRAF, OUTPUT);
  pinMode (DIRAB, OUTPUT);

  pinMode (PWMB, OUTPUT);
  pinMode (DIRBF, OUTPUT);
  pinMode (DIRBB, OUTPUT);

  digitalWrite(DIRAB,0);
  digitalWrite(DIRAF, 1);
  analogWrite(PWMA,pwm);
  
  digitalWrite(DIRBB,0);
  digitalWrite(DIRBF, 1);
  analogWrite(PWMB,pwm);

  pinMode(ENA, OUTPUT);
  digitalWrite(ENA, HIGH);  
  
  /*attachInterrupt(digitalPinToInterrupt(ENCA),readEncoderA,RISING);
  attachInterrupt(digitalPinToInterrupt(ENCB),readEncoderB,RISING);*/
}

void loop() {
  // put your main code here, to run repeatedly:

    unsigned int distancea = 0; 
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
      distancea = distanceia;
      distanceia = 0;
    }

    unsigned int distanceb = 0; 
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
      distanceb = distanceib;
      distanceib = 0;
    }
    
    /*if (distance > target + (ERROR/2) && pwm > 0){
      pwm--;  
      analogWrite(PWMA,pwm);
      analogWrite(PWMB,pwm);
    } else if (distance < target - (ERROR/2) && pwm < 255) {
      pwm++;  
      analogWrite(PWMA,pwm);
      analogWrite(PWMB,pwm);
    }*/

    Serial.print(pwm);
    Serial.print(" : ");
    Serial.print(distancea);
    Serial.print(" : ");
    Serial.print(distanceb);
    Serial.print(" -> ");
    Serial.println(target);
    delay(1000);
}

void readEncoderA(){
    distanceia++;
}

void readEncoderB(){
    distanceib++;
}
