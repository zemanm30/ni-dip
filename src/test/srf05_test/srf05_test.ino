const unsigned int TRIG_PIN2 = 50;
const unsigned int ECHO_PIN2 = 51;

const unsigned int TRIG_PIN = 48;
const unsigned int ECHO_PIN = 49;
const unsigned int BAUD_RATE = 9600;

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(TRIG_PIN2, OUTPUT);
  pinMode(ECHO_PIN2, INPUT);
  Serial.begin(BAUD_RATE);
}


void loop() {

  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);


  const unsigned long duration = pulseIn(ECHO_PIN, HIGH);
  delayMicroseconds(50);
  
  digitalWrite(TRIG_PIN2, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN2, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN2, LOW);

  const unsigned long duration2 = pulseIn(ECHO_PIN2, HIGH);

  int distance = duration / 29 / 2;
  int distance2 = duration2 / 29 / 2;
  if (duration == 0) {
    Serial.println("Warning: no pulse from sensor");
  } else {
    Serial.print(" - distance to nearest object:");
    Serial.print(distance);
    Serial.print(" cm; ");
    
    Serial.print(distance2);
    Serial.println(" cm");
  }
  delay(100);
}