#include <SPI.h>
#include "mcp2515_can.h"

#include "CAN_cfg.h"
#include "reciever_cfg.h"
#include "reciever.c"
#include "HY_SRF05_cfg.h"
#include "HY_SRF05.c"
#include "voltageRead.c"
#include "serialMsg.h"

mcp2515_can CAN(9);  // Set CS pin

volatile unsigned char flagRecv = 0;

//values from reciever
volatile int rudd, elev;

//values from sensors
volatile int sf1, sf2, sf3, sb1, sb2, sb3;

volatile bool RCDisconnect = false;

//CAN msg recieve buffer
unsigned long ID = 0;
unsigned char len = 0;
unsigned char buf[4];

volatile int16_t targetSpeedl = 0;
volatile int16_t targetSpeedr = 0;

//speeds, timeouts and errors for each defice
volatile int16_t speeds[3][2];
volatile long touts[3];
volatile int errors[3];

//system error
int error = 0;

//CAN send buffer
uint8_t sbuf[4];

//Serial buffers for RPI comunication
volatile long serialTout = 0;

void (*resetFunc)(void) = 0;

void setup() {
  Serial.begin(115200);


  //setup reciever
  pinMode(RUDD, INPUT);
  pinMode(ELEV, INPUT);
  pinMode(AILE, INPUT);
  pinMode(THRO, INPUT);

  //setup distance sensors
  initSensor(TRIGG_FRONT_1, ECHO_FRONT_1);
  initSensor(TRIGG_FRONT_2, ECHO_FRONT_2);
  initSensor(TRIGG_FRONT_3, ECHO_FRONT_3);

  initSensor(TRIGG_BACK_1, ECHO_BACK_1);
  initSensor(TRIGG_BACK_2, ECHO_BACK_2);
  initSensor(TRIGG_BACK_3, ECHO_BACK_3);

  //set up CAN
  attachInterrupt(digitalPinToInterrupt(CAN_INT_PIN), MCP2515_ISR, FALLING);
  while (CAN_OK != CAN.begin(CAN_500KBPS)) {
    resetFunc();
    delay(100);
  }

  //set CAN maskas and filters
  CAN.init_Mask(0, 0, CAN_DIR_BIT);
  CAN.init_Filt(0, 0, 0);


  cli();  //stop interrupts
  //set timer1 interrupt at 10Hz
  TCCR1A = 0;  // set entire TCCR1A register to 0
  TCCR1B = 0;  // same for TCCR1B
  TCNT1 = 0;   //initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 6245;  // = (16*10^6) / (10*256) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 bits for 256 prescaler
  TCCR1B |= (1 << CS12);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();  //allow interrupts
}

void loop() {
  //calculate wanted speed

  //check battery status
  if (readBattery(BATTERY_VOLTAGEPIN) < MIN_VOLTAGE) {
    error = 4;
  } else if (error == 4) {
    error = 0;
    byte i = error;
    CAN.sendMsgBuf((CAN_MSG_ERR << 5) | CAN_DIR_BIT, 0, 1, &i);
  }
  //read sensors
  sf1 = getDistance(TRIGG_FRONT_1, ECHO_FRONT_1);
  sf2 = getDistance(TRIGG_FRONT_2, ECHO_FRONT_2);
  sf3 = getDistance(TRIGG_FRONT_3, ECHO_FRONT_3);

  sb1 = getDistance(TRIGG_BACK_1, ECHO_BACK_1);
  sb2 = getDistance(TRIGG_BACK_2, ECHO_BACK_2);
  sb3 = getDistance(TRIGG_BACK_3, ECHO_BACK_3);

  //read reciever
  rudd = RECReadRecieverPin(RUDD);
  elev = RECReadRecieverPin(ELEV);

  if (rudd == -32 || elev == -32) {
    //reciever or RC is disconnected
    RCDisconnect = true;
  } else {
    //move targetspeed to range <-1500, 1500> to get wanted speeds
    int targetSpeedlnew = RECCalculateLeft(rudd, elev) * 75;
    int targetSpeedrnew = RECCalculateRight(rudd, elev) * 75;

    if (targetSpeedlnew >= 0) {
      targetSpeedl = recountSpeed(targetSpeedlnew, sf1, sf2, sf3);
    } else {
      targetSpeedl = recountSpeed(targetSpeedlnew, sb1, sb2, sb3);
    }

    if (targetSpeedrnew >= 0) {
      targetSpeedr = recountSpeed(targetSpeedrnew, sf3, sf2, sf1);
    } else {
      targetSpeedr = recountSpeed(targetSpeedrnew, sb3, sb2, sb1);
    }

    //recount due the obsticles
    RCDisconnect = false;

    if (Serial.available()) {
      int check = Serial.read();
      if (check == SERIAL_STARTING_SYMBOL) {
        int type = Serial.read();
        switch (type) {

          case SERIAL_MSG_ERR:
            check ^= type;
            if (check == Serial.read()) {
              Serial.write(SERIAL_STARTING_SYMBOL);
              Serial.write(SERIAL_MSG_ERR);
              Serial.write(error);
              Serial.write(error ^ SERIAL_STARTING_SYMBOL ^ SERIAL_MSG_ERR);
            }
            break;
          case SERIAL_MSG_ERRS:
            check ^= type;
            if (check == Serial.read()) {
              Serial.write(SERIAL_STARTING_SYMBOL);
              Serial.write(SERIAL_MSG_ERRS);
              Serial.write(errors[0]);
              Serial.write(errors[1]);
              Serial.write(errors[2]);
              Serial.write(errors[0] ^ errors[1] ^ errors[2] ^ SERIAL_STARTING_SYMBOL ^ SERIAL_MSG_ERRS);
            }
            break;

          case SERIAL_MSG_GET_DISTANCES:
            check ^= type;
            if (check == Serial.read()) {
              Serial.write(SERIAL_STARTING_SYMBOL);
              Serial.write(SERIAL_MSG_GET_DISTANCES);
              Serial.write(sf1 & 0xFF);
              Serial.write((sf1 >> 8) & 0xFF);
              Serial.write(sf2 & 0xFF);
              Serial.write((sf2 >> 8) & 0xFF);
              Serial.write(sf3 & 0xFF);
              Serial.write((sf3 >> 8) & 0xFF);
              Serial.write(sb1 & 0xFF);
              Serial.write((sb1 >> 8) & 0xFF);
              Serial.write(sb2 & 0xFF);
              Serial.write((sb2 >> 8) & 0xFF);
              Serial.write(sb3 & 0xFF);
              Serial.write((sb3 >> 8) & 0xFF);
              Serial.write((sf1 & 0xFF) ^ ((sf1 >> 8) & 0xFF) ^ (sf2 & 0xFF) ^ ((sf2 >> 8) & 0xFF) ^ (sf3 & 0xFF) ^ ((sf3 >> 8) & 0xFF) ^ (sb1 & 0xFF) ^ ((sb1 >> 8) & 0xFF) ^ (sb2 & 0xFF) ^ ((sb2 >> 8) & 0xFF) ^ (sb3 & 0xFF) ^ ((sb3 >> 8) & 0xFF) ^ SERIAL_STARTING_SYMBOL ^ SERIAL_MSG_GET_DISTANCES);
            }
            break;
          case SERIAL_MSG_GET_SPEEDS:
            check ^= type;
            if (check == Serial.read()) {
              Serial.write(SERIAL_STARTING_SYMBOL);
              Serial.write(SERIAL_MSG_GET_SPEEDS);
              Serial.write(targetSpeedl & 0xFF);
              Serial.write((targetSpeedl >> 8) & 0xFF);
              Serial.write(targetSpeedr & 0xFF);
              Serial.write((targetSpeedr >> 8) & 0xFF);
              Serial.write((targetSpeedl & 0xFF) ^ ((targetSpeedl >> 8) & 0xFF) ^ (targetSpeedr & 0xFF) ^ ((targetSpeedr >> 8) & 0xFF) ^ SERIAL_STARTING_SYMBOL ^ SERIAL_MSG_GET_SPEEDS);
            }
            break;

          case SERIAL_MSG_SET_SPEEDS:
            check ^= type;
            if (RCDisconnect) {
              int Speedl = Serial.read();
              int Speedr = Serial.read();
              check ^= Speedl ^ Speedr;
              if (Speedl >= -100 && Speedl <= 100 && Speedr >= -100 && Speedr && check == Serial.read()) {
                if (Speedl >= 0) {
                  targetSpeedl = recountSpeed(Speedl * 15, sf1, sf2, sf3);
                } else {
                  targetSpeedl = recountSpeed(Speedl * 15, sb1, sb2, sb3);
                }

                if (Speedr >= 0) {
                  targetSpeedr = recountSpeed(Speedr * 15, sf3, sf2, sf1);
                } else {
                  targetSpeedr = recountSpeed(Speedr * 15, sb3, sb2, sb1);
                }

              Serial.write(SERIAL_STARTING_SYMBOL);
              Serial.write(SERIAL_MSG_SET_SPEEDS);
              Serial.write(targetSpeedl & 0xFF);
              Serial.write((targetSpeedl >> 8) & 0xFF);
              Serial.write(targetSpeedr & 0xFF);
              Serial.write((targetSpeedr >> 8) & 0xFF);
              Serial.write((targetSpeedl & 0xFF) ^ ((targetSpeedl >> 8) & 0xFF) ^ (targetSpeedr & 0xFF) ^ ((targetSpeedr >> 8) & 0xFF) ^ SERIAL_STARTING_SYMBOL ^ SERIAL_MSG_SET_SPEEDS);  
              serialTout = 0;              
              }
            }
            break;

          case SERIAL_MSG_LIVE:
            check ^= type;
            if (check == Serial.read()) {
              Serial.write(SERIAL_STARTING_SYMBOL);
              Serial.write(SERIAL_MSG_LIVE);
              Serial.write(SERIAL_STARTING_SYMBOL ^ SERIAL_MSG_LIVE);
              serialTout = 0;
            }
            break;
        }
      }
    }
  }
}

void MCP2515_ISR() {
  while (CAN_MSGAVAIL == CAN.checkReceive()) {
    CAN.readMsgBufID(&ID, &len, buf);

    if ((ID & 0xF) != 0) {
      touts[(ID & 0xF) - 1] = 0;
      switch ((ID) >> 5) {
        case CAN_MSG_SSPEED:
          for (int i = 0; i < 2; i++) {
            speeds[(ID & 0xF) - 1][i] = ((buf[i * 2]) & (buf[(i * 2) + 1]));
          }
          break;
        case CAN_MSG_ERR:
          if (len == 1) {
            errors[(ID & 0xF) - 1] = buf[1];

            for (int i = 0; i < 3; i++) {
              if (errors[i] != 0 && errors[i] != 2) {
                error = 5;
              }
            }
          }
          break;
      }
    }
  }
}


ISR(TIMER1_COMPA_vect) {
  //calculate timeouts
  for (int i = 0; i < 3; i++) {
    if (touts[i] < CAN_TIMEOUT_MS) {
      touts[i] += 10;

      if (errors[i] == 3) {
        errors[i] = 0;
      }

    } else {
      errors[i] = 3;
      error = 3;
    }
  }

  if (serialTout < SERIAL_T_OUT)
  {
    serialTout += 10;
  }

  if (errors[0] == 0 && errors[1] == 0 && errors[2] == 0) {
    error = 0;
    byte i = 0;
    CAN.sendMsgBuf((CAN_MSG_ERR << 5) | CAN_DIR_BIT, 0, 1, &i);
  }

  //send speeds to can
  if (error != 0) {
    Serial.println(error);
    CAN.sendMsgBuf((CAN_MSG_ERR << 5) | CAN_DIR_BIT, 0, 0, sbuf);
  } else if ((targetSpeedl == 0 && targetSpeedr == 0) || ( RCDisconnect && (serialTout >= SERIAL_T_OUT) )) {
    CAN.sendMsgBuf((CAN_MSG_STOP << 5) | CAN_DIR_BIT, 0, 0, sbuf);
  } else {
    sbuf[0] = (targetSpeedl & 0xFF);
    sbuf[1] = ((targetSpeedl >> 8) & 0xFF);
    sbuf[2] = (targetSpeedr & 0xFF);
    sbuf[3] = ((targetSpeedr >> 8) & 0xFF);
    CAN.sendMsgBuf((CAN_MSG_SSPEED << 5) | CAN_DIR_BIT, 0, 4, sbuf);
  }
}