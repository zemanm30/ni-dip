
#ifndef __CAN_CFG_H__
#define __CAN_CFG_H__

//CAN header positions
#define CAN_DIR_BIT 0x0010
#define CAN_ADDR_BITS 0x000F
#define CAN_MSG_TYPE 0x07E0

//CAN msg types
#define CAN_MSG_STOP 0x00
#define CAN_MSG_ERR 0x01
#define CAN_MSG_SSPEED 0x04

//CAN HW config
const int CAN_INT_PIN = 2;

//timeout to error
const long CAN_TIMEOUT_MS = 30;

#endif