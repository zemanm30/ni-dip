
#ifndef __VOLTAGEREAD_C__
#define __VOLTAGEREAD_C__

#define BATTERY_VOLTAGEPIN A0
#define MIN_VOLTAGE 11.0
 /**
  * returns voltage recounted from preassumed voltage devider 1/4
  */
float readBattery(int pin)
{
  return (((float)analogRead(pin)) / 1024.0) * 20.0;
}
#endif