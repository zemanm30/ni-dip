
//input encoder pins
#define ENCLA 18
#define ENCLB 19
#define ENCRA 20
#define ENCRB 21

//motora pins
#define PWMA 5
#define DIRAF 8
#define DIRAB 7

#define PWMB 6
#define DIRBF 4
#define DIRBB 10

#define DIAGL A2
#define DIAGR A3