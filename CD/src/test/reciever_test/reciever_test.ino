#define RUDD 3
#define ELEV 4
#define AILE 5
#define THRO 6
int rudd, elev, aile, thro;


void setup() {
  Serial.begin(9600);
  pinMode(RUDD, INPUT);
  pinMode(ELEV, INPUT);
  pinMode(AILE, INPUT);
  pinMode(THRO, INPUT);
}

void loop() {
  int ruddin = pulseIn(RUDD, HIGH,50000);
  Serial.println(ruddin);
  rudd = ((ruddin / 10) - 145) / 2;
  elev = ((pulseIn(ELEV, HIGH) / 10) - 145) / 2;
  aile = ((pulseIn(AILE, HIGH) / 10) - 145) / 2;
  thro = ((pulseIn(THRO, HIGH) / 10) - 145) / 2;

  Serial.print(rudd);
  Serial.print(" : ");
  Serial.print(elev);
  Serial.print(" : ");
  Serial.print(aile);
  Serial.print(" : ");
  Serial.println(thro);


  int dist = min(sqrt(rudd * rudd + elev * elev), 20);
  if (elev < 0) {
    dist *= -1;
  }

  
  if (rudd >= 0 && elev >= 0) {
    Serial.print(dist);
    Serial.print(" : ");
    Serial.println(elev - rudd);
  }
  else if (rudd < 0 && elev >= 0) {
    Serial.print(elev + rudd);
    Serial.print(" : ");        
    Serial.println(dist);
  }else if (rudd >= 0 && elev < 0) {
    Serial.print(dist);
    Serial.print(" : ");
    Serial.println(elev + rudd);
  } else {
    Serial.print(rudd - elev);
    Serial.print(" : ");        
    Serial.println(dist);    
  }
  
}