#include <util/atomic.h>

//input encoder pins
#define ENCLA   18
#define ENCLB   19
#define ENCRA   20
#define ENCRB   21

//motora pins
#define PWMA  6
#define DIRAF 4
#define DIRAB 5

#define PWMB  10
#define DIRBF 8
#define DIRBB 7


volatile int posa = 0;
volatile int posb = 0;

volatile long speeda = 0;
volatile long speedb = 0;

int pwma = 100;
int pwmb = 100;

volatile int targetspeeda = 0;
volatile int targetspeedb = 0;

void setup() {
  //serial
  Serial.begin(9600);

  //initialize inputs
  pinMode(ENCLA, INPUT);
  pinMode(ENCLB, INPUT);
  pinMode(ENCRA, INPUT);
  pinMode(ENCRB, INPUT);


  pinMode (PWMA, OUTPUT);
  pinMode (DIRAF, OUTPUT);
  pinMode (DIRAB, OUTPUT);

  pinMode (PWMB, OUTPUT);
  pinMode (DIRBF, OUTPUT);
  pinMode (DIRBB, OUTPUT);

  digitalWrite(DIRAF, 0);
  digitalWrite(DIRAB, 0);
  analogWrite(PWMA, 0);

  digitalWrite(DIRBF, 0);
  digitalWrite(DIRBB, 0);
  analogWrite(PWMB, 0);

  //attach encoder interrupts
  attachInterrupt(digitalPinToInterrupt(ENCLA), readLEncoder, RISING);
  attachInterrupt(digitalPinToInterrupt(ENCRA), readREncoder, RISING);

  cli();//stop interrupts
  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 6249;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();//allow interrupts
}

void loop() {
  // put your main code here, to run repeatedly:

  delay(1000);

  if (Serial.available() > 2) {
    //set speeds
    targetspeeda = Serial.parseInt();
    targetspeedb = Serial.parseInt();
    Serial.println("New speeds");
  }

  int apos = 0;
  int bpos = 0;

  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    apos = speeda;
    bpos = speedb;
  }


  Serial.print(pwma);
  Serial.print(" x ");
  Serial.print(targetspeeda);
  Serial.print(" -> ");
  Serial.print(apos);
  Serial.print(":");
  Serial.print(pwmb);
  Serial.print(" x ");
  Serial.print(targetspeedb);
  Serial.print(" -> ");
  Serial.println(bpos);
}

ISR (TIMER1_COMPA_vect)
{
  speeda = posa * 10;
  speedb = posb * 10;
  posa = 0;
  posb = 0;

  //set dira
  if (targetspeeda > 0)
  {
    digitalWrite(DIRAF, 0);
    digitalWrite(DIRAB, 1);
  }
  else if (targetspeeda < 0)
  {
    digitalWrite(DIRAF, 1);
    digitalWrite(DIRAB, 0);
  }
  else
  {
    digitalWrite(DIRAF, 0);
    digitalWrite(DIRAB, 0);
  }

  if (abs(speeda) < abs(targetspeeda))
  {
    if (abs(abs(speeda) - abs(targetspeeda)) > 200) {
      pwma += 10;
    }
    if (abs(abs(speeda) - abs(targetspeeda)) > 50)
    {
      pwma++;
    }
    if (pwma > 255)
    {
      pwma = 255;
    }
  }
  else if (abs(speeda) > abs(targetspeeda))
  {
    if (abs(abs(speeda) - abs(targetspeeda)) > 200) {
      pwma -= 10;
    }
    else if (abs(abs(speeda) - abs(targetspeeda)) > 50)
    {
      pwma--;
    }
    if (pwma < 0)
    {
      pwma = 0;
    }
  }

  analogWrite(PWMA, pwma);

  if (targetspeedb > 0)
  {
    digitalWrite(DIRBF, 0);
    digitalWrite(DIRBB, 1);
  }
  else if (targetspeedb < 0)
  {
    digitalWrite(DIRBF, 1);
    digitalWrite(DIRBB, 0);
  }
  else
  {
    digitalWrite(DIRBF, 0);
    digitalWrite(DIRBB, 0);
  }

  if (abs(speedb) < abs(targetspeedb))
  {
    if (abs(abs(speedb) - abs(targetspeedb)) > 200) {
      pwmb += 10;
    }
    else if (abs(abs(speedb) - abs(targetspeedb)) > 50)
    {
      pwmb++;
    }
    if (pwmb > 255)
    {
      pwmb = 255;
    }
  }
  else if (abs(speedb) > abs(targetspeedb))
  {
    if (abs(abs(speedb) - abs(targetspeedb)) > 200) {
      pwmb -= 10;
    }
    else if (abs(abs(speedb) - abs(targetspeedb)) > 50)
    {
      pwmb--;
    }
    if (pwmb < 0)
    {
      pwmb = 0;
    }
  }
  analogWrite(PWMB, pwmb);

}

void readLEncoder ()  {
  int b = digitalRead(ENCLB);
  if (b > 0) {
    posa++;
  }
  else {
    posa--;
  }
}

void readREncoder ()  {
  int b = digitalRead(ENCRB);
  if (b > 0) {
    posb++;
  }
  else {
    posb--;
  }
}
