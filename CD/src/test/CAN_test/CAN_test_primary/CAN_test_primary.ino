#include <SPI.h>

#include "mcp2515_can.h"

//CAN header positions
#define CAN_DIR_BIT 0x0100
#define CAN_ADDR_BITS 0x000F


//CAN seeeduino parameters
const int CAN_INT_PIN = 2;
mcp2515_can CAN(9); // Set CS pin

volatile unsigned char flagRecv = 0;
volatile unsigned char flagSec = 0;

void setup() {
  
    cli(); //stop interrupts
    
    //set timer1 interrupt at 1Hz
    TCCR1A = 0;// set entire TCCR1A register to 0
    TCCR1B = 0;// same for TCCR1B
    TCNT1  = 0;//initialize counter value to 0
    // set compare match register for 1hz increments
    OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (must be <65536)
    // turn on CTC mode
    TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler
    TCCR1B |= (1 << CS12) | (1 << CS10);  
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
    
    sei(); //allow interrupts
    
    //Setup serial
    Serial.begin(115200);
    while(!Serial);

    //Setup CAN
    attachInterrupt(digitalPinToInterrupt(CAN_INT_PIN), MCP2515_ISR, FALLING); // start interrupt
    
    while (CAN_OK != CAN.begin(CAN_500KBPS)) {             // init can bus : baudrate = 500k
        Serial.println("CAN BUS Shield init fail");
        Serial.println(" Init CAN BUS Shield again");
        delay(100);
    }
    Serial.println("CAN BUS Shield init ok!");
    CAN.init_Mask(0, 0, CAN_DIR_BIT);
    CAN.init_Filt(0, 0, 0x00);

}

void MCP2515_ISR() {
    flagRecv = 1;
}

ISR(TIMER1_COMPA_vect){
    flagSec = 1;
}

unsigned char len = 0;
unsigned char stmp[3][2] = {{0, 0},{0, 0},{0, 0}};
unsigned char buff[2] = {0, 0};

void loop() {
  // put your main code here, to run repeatedly:
  //generate random number 

  //check if receieved
  if (flagRecv)
  {
     flagRecv = 0;                   // clear flag
     // iterate over all pending messages
     // If either the bus is saturated or the MCU is busy,
     // both RX buffers may be in use and reading a single
     // message does not clear the IRQ conditon.
     while (CAN_MSGAVAIL == CAN.checkReceive()) {
          // read data,  len: data length, buf: data buf
          unsigned long ID = 0;
          CAN.readMsgBufID(&ID, &len, buff);

          stmp[ID - 1][0] = buff[0];
          
          stmp[ID - 1][1] = buff[1];
          
          //process msg
     }
  }

  if(flagSec){
    //send it
    CAN.sendMsgBuf(CAN_DIR_BIT, 0, 2, stmp[0]);

  
    Serial.print(stmp[0][0]);
    Serial.print(": ");
    Serial.println(stmp[0][1]);
  
    Serial.print(stmp[1][0]);
    Serial.print(": ");
    Serial.println(stmp[1][1]);  
  
    Serial.print(stmp[2][0]);
    Serial.print(": ");
    Serial.println(stmp[2][1]);
    Serial.println("____");

    flagSec = 0;
  }
}
