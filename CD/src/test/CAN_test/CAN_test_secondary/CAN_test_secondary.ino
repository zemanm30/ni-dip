#include <SPI.h>

#include "mcp2515_can.h"

//CAN header positions
#define CAN_DIR_BIT 0x0100
#define CAN_ADDR_BITS 0x000F

//mask for rhe slave
const uint16_t slave_mask = CAN_DIR_BIT | CAN_ADDR_BITS;

//slave address, can't be 0 -> would be master
const uint16_t addr = 3;

//CAN seeeduino parameters
const int CAN_INT_PIN = 2;
mcp2515_can CAN(9); // Set CS pin

volatile unsigned char flagRecv = 0;
volatile unsigned char flagSec = 0;

void setup() {
  
    cli(); //stop interrupts
    
    //set timer1 interrupt at 1Hz
    TCCR1A = 0;// set entire TCCR1A register to 0
    TCCR1B = 0;// same for TCCR1B
    TCNT1  = 0;//initialize counter value to 0
    // set compare match register for 1hz increments
    OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (must be <65536)
    // turn on CTC mode
    TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler
    TCCR1B |= (1 << CS12) | (1 << CS10);  
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
    
    sei(); //allow interrupts
  
    //Setup serial
    Serial.begin(115200);
    while(!Serial);

    //Setup CAN
    attachInterrupt(digitalPinToInterrupt(CAN_INT_PIN), MCP2515_ISR, FALLING); // start interrupt
    
    while (CAN_OK != CAN.begin(CAN_500KBPS)) {             // init can bus : baudrate = 500k
        Serial.println("CAN BUS Shield init fail");
        Serial.println(" Init CAN BUS Shield again");
        delay(100);
    }
    Serial.println("CAN BUS Shield init ok!");
    CAN.init_CS(9);
    
    CAN.init_Mask(0, 0, slave_mask);
    CAN.init_Filt(0, 0, CAN_DIR_BIT);
    CAN.init_Filt(1, 0, CAN_DIR_BIT | (CAN_ADDR_BITS & addr));

}

void MCP2515_ISR() {
    flagRecv = 1;
}
 
ISR(TIMER1_COMPA_vect){
    flagSec = 1;
}

unsigned char stmp[8] = {0, 0, 0, 0, 0, 0, 0, 0};

unsigned char len = 0;
unsigned char buff[8];

void loop() {
  // put your main code here, to run repeatedly:
  //generate random number 

  

  //check if receieved
  if (flagRecv)
  {
    //CAN.readMsgBuf( 1, &stmp[1] );
    stmp[1] ++;
    //Serial.println("Receieved :");
    flagRecv = 0;
    while (CAN_MSGAVAIL == CAN.checkReceive()) {
          // read data,  len: data length, buf: data buf
          CAN.readMsgBuf(&len, buff);

          //process msg
     }
  }
  
  if(flagSec){
    //send it
    stmp[0] = random(200);
    
    CAN.sendMsgBuf( (CAN_ADDR_BITS & addr) , 0, 2, stmp);
  
    Serial.print(stmp[0]);
    Serial.print(": ");
    Serial.println(stmp[1]);
    delay(1000);
    
    flagSec = 0;
  }
}
