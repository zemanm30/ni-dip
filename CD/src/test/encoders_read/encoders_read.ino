#include <util/atomic.h>

//input encoder pins
#define ENCLA   18
#define ENCLB   19
#define ENCRA   20
#define ENCRB   21


volatile int posl = 0;
volatile int posr = 0;

void setup() {
  //serial
  Serial.begin(9600);

  //initialize inputs
  pinMode(ENCLA,INPUT);
  pinMode(ENCLB,INPUT);
  pinMode(ENCRA,INPUT);
  pinMode(ENCRB,INPUT);

  //attach encoder interrupts
  attachInterrupt(digitalPinToInterrupt(ENCLA),readLEncoder,RISING);
  attachInterrupt(digitalPinToInterrupt(ENCRA),readREncoder,RISING);
}

void loop() {
  // put your main code here, to run repeatedly:

  delay(1000);
  
  int lpos = 0;
  int rpos = 0; 
   
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    lpos = posl;
    rpos = posr;
  }
  
  Serial.print(lpos);
  Serial.print(":");
  Serial.println(rpos);
}

void readLEncoder ()  {
  int b = digitalRead(ENCLB);
  if(b > 0){
    posl++;
  }
  else{
    posl--;
  }
}

void readREncoder ()  {
  int b = digitalRead(ENCRB);
  if(b > 0){
    posr++;
  }
  else{
    posr--;
  }
}
