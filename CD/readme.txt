
Složka src obsahuje zdrojeové kódy, které vznikly při práci.
Ve složce deploy je finální kód pro prototip. Na prototypu jsou
použity rozdílné kódy pro primární a sekundární moduly, proto
obsahuje tyto dva kódy. Kód je určený pro Arduino IDE. 
U sekundárního modulu je přidaná duležitá poznámka k jeho použití.
K jejich použití je třeba mít v Arduino IDE nainstalovanou knihovnu
pro CAN, dle návodu na zprovoznění 
zde https://wiki.seeedstudio.com/CAN-BUS_Shield_V2.0/.

Ve složce src/test jsou uloženy testovací kódy pouřité pro testování 
jednotlivých komponent. Kód je opět určen pro Arduino IDE.

Ve složce src/scad jsou uloženy kód k 3D modelům vzniklých během
projektu. Kódy jsou určeny pro program Oprncad.

Ve složce zemanm30 a v archivu zemanm30.zip jsou uloženy zdrojové 
kódy LaTeX-ového dokumentu, který popisuje projekt.

Soubor zemanm30.pdf je zkompilovaný LaTeX-ový projekt.

Ve složce protocol jsou uloženy obsahy zpráv pro užití nadřazeného modulu.
To je obsaženo v souboru USB protocol.xlsx